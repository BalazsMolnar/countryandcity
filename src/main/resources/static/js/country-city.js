$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
	*/

	$('body').on('click', '#add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
			elvileg megoldva
		*/
		var cityData = {
			/*
				TODO: összeszedni az űrlap adatait
			*/
		};
		$.ajax({
			url: '/rest/cities',
			method : 'POST',
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
			}
		});
	});

	/*
	$('tbody').on('click', 'a', function(){
		$.ajax({
			url: '/country/',
			method : 'GET',
			data : cityData,
			dataType : 'json',
			complete : function(){
				
			}
		});
	});
	*/

	/*
		TODO: ország törlés gombok működjenek
	*/
	$('tbody').on('click', '.delete-country', function(){
		// TODO confirmation?
		$(this).closest('tr').remove();
	});

	/*
		TODO: város törlés gombok működjenek
	*/
	$('tbody').on('click', '.delete-city', function(){
		// TODO confirmation?
		$(this).closest('tr').remove();
	});

});